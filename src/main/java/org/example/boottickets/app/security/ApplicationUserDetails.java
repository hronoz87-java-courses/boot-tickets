package org.example.boottickets.app.security;

import lombok.*;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

@NoArgsConstructor
@AllArgsConstructor
@Data
@With
public class ApplicationUserDetails implements UserDetails, CredentialsContainer {
    private long id;
    private String username;
    private String password;
    private Collection<? extends GrantedAuthority> authorities = Collections.emptySet();
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    @Override
    public void eraseCredentials() {
        password = "***";
    }

    @Getter
    @Value
    @EqualsAndHashCode(callSuper = false)
    public static class Anonymous extends ApplicationUserDetails {
        private Anonymous() {
        }

        private static final Anonymous instance = new Anonymous();
        long id = -1;
        String username = "anonymous";
        String password = "***";

        @Override
        public void setId(final long id) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setUsername(final String username) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setPassword(final String password) {
            throw new UnsupportedOperationException();
        }
    }
}
