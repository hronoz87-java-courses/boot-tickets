package org.example.boottickets.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.boottickets.app.validation.NotForbiddenValue;
import org.example.boottickets.app.validation.NotForbiddenValueRegex;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketCreateRQ {
    @NotForbiddenValue({"root", "admin", "support"})
    @NotNull
    @Size(min = 3, max = 20)
    private String name;
    @NotNull
    @Size(min = 1, max = 200)
    @NotForbiddenValueRegex
    private String content;
}
