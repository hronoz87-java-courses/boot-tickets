package org.example.boottickets.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentCreateRS {
    private Long id;
    private String author;
    private String text;
    private Long ticketId;
}
