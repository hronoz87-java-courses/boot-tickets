package org.example.boottickets.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.boottickets.app.validation.NotForbiddenValue;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserRQ {
  private long id;
  @NotForbiddenValue({"root", "admin"})
  @NotNull
  @Size(min = 3, max = 20)
  private String login;
  @NotNull
  @Size(min = 5, max = 64)
  private String password;
}
