package org.example.boottickets.app.error.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ErrorRS {
  public static final String UNKNOWN_ERROR = "err.internal.unknown";
  public static final String VALIDATION_ERROR = "err.validation.invalid";
  public static final String NOT_FOUND = "err.not_found.unknown";
  public static final String FORBIDDEN = "err.forbidden.invalid";

  private String code;
}
