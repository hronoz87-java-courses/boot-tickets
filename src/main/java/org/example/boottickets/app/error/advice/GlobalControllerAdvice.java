package org.example.boottickets.app.error.advice;


import org.example.boottickets.app.error.dto.ErrorRS;
import org.example.boottickets.app.exception.NotTicketWithIdException;
import org.example.boottickets.app.exception.RoleNotSupportException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class GlobalControllerAdvice {
  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorRS exception(final MethodArgumentNotValidException e) {
    e.printStackTrace();
    return new ErrorRS(ErrorRS.VALIDATION_ERROR);
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorRS exception(final ConstraintViolationException e) {
    e.printStackTrace();
    return new ErrorRS(ErrorRS.VALIDATION_ERROR);
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorRS exception(final NotTicketWithIdException e) {
    e.printStackTrace();
    return new ErrorRS(ErrorRS.NOT_FOUND);
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.FORBIDDEN)
  public ErrorRS exception(final RoleNotSupportException e) {
    e.printStackTrace();
    return new ErrorRS(ErrorRS.FORBIDDEN);
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorRS exception(final Throwable e) {
    e.printStackTrace();
    return new ErrorRS(ErrorRS.UNKNOWN_ERROR);
  }
}
