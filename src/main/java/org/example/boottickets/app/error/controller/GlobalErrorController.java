package org.example.boottickets.app.error.controller;

import org.example.boottickets.app.error.dto.ErrorRS;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("${server.error.path:${error.path:/error}}")
public class GlobalErrorController extends AbstractErrorController {
  private final ErrorAttributes errorAttributes;

  public GlobalErrorController(ErrorAttributes errorAttributes) {
    super(errorAttributes);
    this.errorAttributes = errorAttributes;
  }

  @RequestMapping
  public ResponseEntity<ErrorRS> error(final HttpServletRequest request, final WebRequest webRequest) {
    final HttpStatus status = getStatus(request);
    final Map<String, Object> errorAttributes = this.errorAttributes.getErrorAttributes(webRequest, ErrorAttributeOptions.defaults());
    final Throwable error = this.errorAttributes.getError(webRequest);
    if (error == null) {
      error.printStackTrace();
    }

    return ResponseEntity.status(status).body(new ErrorRS("..."));
  }
}
