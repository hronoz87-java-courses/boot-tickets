package org.example.boottickets.app.exception;

public class RoleNotSupportException extends ApplicationException {

    public RoleNotSupportException() {
    }

    public RoleNotSupportException(String message) {
        super(message);
    }

    public RoleNotSupportException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoleNotSupportException(Throwable cause) {
        super(cause);
    }

    public RoleNotSupportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
