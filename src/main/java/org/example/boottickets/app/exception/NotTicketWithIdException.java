package org.example.boottickets.app.exception;

public class NotTicketWithIdException  extends ApplicationException{

    public NotTicketWithIdException() {
    }

    public NotTicketWithIdException(String message) {
        super(message);
    }

    public NotTicketWithIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotTicketWithIdException(Throwable cause) {
        super(cause);
    }

    public NotTicketWithIdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
