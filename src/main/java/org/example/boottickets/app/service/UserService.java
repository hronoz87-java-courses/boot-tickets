package org.example.boottickets.app.service;

import lombok.RequiredArgsConstructor;
import org.example.boottickets.app.entity.UserEntity;
import org.example.boottickets.app.mapper.UserEntityMapper;
import org.example.boottickets.app.repository.UserRepository;
import org.example.boottickets.app.security.ApplicationUserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository repository;
    private final UserEntityMapper mapper;

    @Override
    public ApplicationUserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final UserEntity saved = repository.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return mapper.toUserDetails(saved);
    }
}
