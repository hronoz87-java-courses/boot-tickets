package org.example.boottickets.app.service;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.example.boottickets.app.dto.*;
import org.example.boottickets.app.entity.CommentEntity;
import org.example.boottickets.app.entity.TicketEntity;
import org.example.boottickets.app.entity.UserEntity;
import org.example.boottickets.app.exception.NotTicketWithIdException;
import org.example.boottickets.app.exception.RoleNotSupportException;
import org.example.boottickets.app.mapper.CommentMapper;
import org.example.boottickets.app.mapper.TicketMapper;
import org.example.boottickets.app.repository.CommentRepository;
import org.example.boottickets.app.repository.TicketRepository;
import org.example.boottickets.app.repository.UserRepository;
import org.example.boottickets.app.security.ApplicationUserDetails;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.example.boottickets.app.service.NameRoleAndStatus.SUPPORT;

@Transactional
@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final TicketMapper ticketMapper;
    private final UserRepository userRepository;
    private final TicketRepository ticketRepository;
    private final Gson gson;

    public List<TicketGetAllRS> getAllTickets(final ApplicationUserDetails principal, final int pageNo, final int limit) {
        checkSupportRole(principal);
        final List<TicketEntity> ticketEntities = ticketRepository.findAll(PageRequest.of(pageNo, limit, Sort.Direction.ASC, "id")).toList();
        return commentMapper.toListOfTicketGetByLogin(ticketEntities);
    }

    @Transactional(readOnly = true)
    public TicketCreateRS getTicketById(final ApplicationUserDetails principal, final Long id) throws NotTicketWithIdException {
        checkSupportRole(principal);
        final TicketEntity ticket = ticketRepository.findById(id)
                .orElseThrow(() -> new NotTicketWithIdException("ticket with id: " + id + " not found"));
        final List<CommentEntity> allComments = commentRepository.findAllComments(ticket.getId());
        final List<CommentGetAllRS> commentGetAllRS = commentMapper.toListComments(allComments);
        ticket.setComments(gson.toJson(commentGetAllRS));
        return ticketMapper.toTicketCreateRS(ticket);
    }


    public CommentCreateRS createComment(final ApplicationUserDetails principal, final CommentCreateRQ req, final Long id) {
        checkSupportRole(principal);
        CommentEntity commentEntity = commentMapper.fromCommentRQ(
                req,
                req.getAuthor(),
                req.getText()
        );
        final Optional<TicketEntity> ticketEntity = ticketRepository.findById(id);
        commentEntity.setTicketId(ticketEntity.get().getId());
        final CommentEntity saved = commentRepository.save(commentEntity);
        ticketEntity.get().setComments(String.valueOf(commentEntity));
        ticketRepository.save(ticketEntity.get());
        return commentMapper.toCommentRS(saved);
    }

    @Transactional(readOnly = true)
    public void checkSupportRole(final ApplicationUserDetails principal) {
        final Optional<UserEntity> userEntity = userRepository.findByLogin(principal.getUsername());
        final String role = userEntity.get().getRole();
        if (role.equals(SUPPORT)) {
            return;
        }
        throw new RoleNotSupportException(principal.getUsername());
    }

}
