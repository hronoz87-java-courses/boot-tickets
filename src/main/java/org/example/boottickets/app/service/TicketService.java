package org.example.boottickets.app.service;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.example.boottickets.app.dto.*;
import org.example.boottickets.app.entity.CommentEntity;
import org.example.boottickets.app.entity.TicketEntity;
import org.example.boottickets.app.mapper.CommentMapper;
import org.example.boottickets.app.mapper.TicketMapper;
import org.example.boottickets.app.repository.CommentRepository;
import org.example.boottickets.app.repository.TicketRepository;
import org.example.boottickets.app.security.ApplicationUserDetails;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.example.boottickets.app.service.NameRoleAndStatus.CLOSE;

@Transactional
@Service
@RequiredArgsConstructor
public class TicketService {
    private final TicketRepository ticketRepository;
    private final TicketMapper ticketMapper;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final Gson gson;

    public TicketCreateRS create(final ApplicationUserDetails principal, final TicketCreateRQ req) {
        final TicketEntity ticketEntity = ticketMapper.fromTicketCreateRQ(
                req,
                req.getName(),
                req.getContent()
        );
        ticketEntity.setUserId(principal.getId());
        final TicketEntity saved = ticketRepository.save(ticketEntity);
        return ticketMapper.toTicketCreateRS(saved);
    }

    @Transactional(readOnly = true)
    public List<TicketGetAllRS> getAllTickets(final ApplicationUserDetails principal, final int pageNo, final int limit) {
        final List<TicketEntity> tickets = ticketRepository.getAllByUserId(principal.getId(),
                PageRequest.of(pageNo, limit, Sort.Direction.ASC, "id")).toList();
        return ticketMapper.toListOfTicketGetByLogin(tickets);
    }

    @Transactional(readOnly = true)
    public TicketCreateRS getTicketById(final ApplicationUserDetails principal, final Long ticketId) {
        final TicketEntity ticket = ticketRepository.findTicketEntityById(principal.getId(), ticketId);
        final List<CommentEntity> allComments = commentRepository.findAllComments(ticket.getId());
        final List<CommentGetAllRS> commentGetAllRS = commentMapper.toListComments(allComments);
        ticket.setComments(gson.toJson(commentGetAllRS));
        return ticketMapper.toTicketCreateRS(ticket);
    }

    public TicketCreateRS closeById(final ApplicationUserDetails principal, final Long ticketId) {
        final TicketEntity ticket = ticketRepository.findTicketEntityById(principal.getId(), ticketId);
        ticket.setStatus(CLOSE);
        return ticketMapper.toTicketCreateRS(ticket);
    }
}
