package org.example.boottickets.app.repository;

import org.example.boottickets.app.entity.TicketEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface TicketRepository extends CrudRepository<TicketEntity, Long> {

    @Query("SELECT t FROM TicketEntity t WHERE t.userId = :userId AND t.id =:ticketId")
    TicketEntity findTicketEntityById(
            @Param("userId") Long id,
            @Param("ticketId") Long ticketId
    );

    Page<TicketEntity> getAllByUserId(final long id, final Pageable pageable);

    Page<TicketEntity> findAll(final Pageable pageable);
}