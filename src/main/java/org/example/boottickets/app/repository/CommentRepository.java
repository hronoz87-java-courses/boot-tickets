package org.example.boottickets.app.repository;

import org.example.boottickets.app.entity.CommentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CommentRepository extends CrudRepository<CommentEntity, Long> {

    @Query("SELECT c FROM CommentEntity c WHERE c.ticketId =:id")
    List<CommentEntity> findAllComments(
            @Param("id") final Long id);
}
