package org.example.boottickets.app.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class NotForbiddenValueValidator implements ConstraintValidator<NotForbiddenValue, String> {
  private List<String> forbidden;

  @Override
  public void initialize(final NotForbiddenValue constraintAnnotation) {
    forbidden = Arrays.asList(constraintAnnotation.value());
  }

  @Override
  public boolean isValid(final String value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }

    return !forbidden.contains(value.trim().toLowerCase());
  }
}
