package org.example.boottickets.app.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class NotForbiddenValueValidatorRegex implements ConstraintValidator<NotForbiddenValueRegex, String> {

    private static final String CHECK_REGEX = "^[0-9]*$";

    @Override
    public void initialize(NotForbiddenValueRegex constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null) {
            return true;
        }

        final boolean matches = Pattern.matches(CHECK_REGEX, value);
        return !matches;
    }
}
