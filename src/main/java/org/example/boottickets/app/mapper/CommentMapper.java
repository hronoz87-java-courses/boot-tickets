package org.example.boottickets.app.mapper;


import org.example.boottickets.app.dto.CommentCreateRQ;
import org.example.boottickets.app.dto.CommentCreateRS;
import org.example.boottickets.app.dto.CommentGetAllRS;
import org.example.boottickets.app.dto.TicketGetAllRS;
import org.example.boottickets.app.entity.CommentEntity;
import org.example.boottickets.app.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface CommentMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "ticketId", ignore = true)
    @Mapping(target = "created", ignore = true)
    CommentEntity fromCommentRQ(
            final CommentCreateRQ req,
            final String author,
            final String text
    );

    CommentCreateRS toCommentRS(final CommentEntity comment);

    @Mapping(target = "comment", ignore = true)
    List<TicketGetAllRS> toListOfTicketGetByLogin(final List<TicketEntity> tickets);


    List<CommentGetAllRS> toListComments(final List<CommentEntity> comments);
}
