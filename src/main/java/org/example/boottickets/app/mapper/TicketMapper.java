package org.example.boottickets.app.mapper;

import org.example.boottickets.app.dto.TicketCreateRQ;
import org.example.boottickets.app.dto.TicketCreateRS;
import org.example.boottickets.app.dto.TicketGetAllRS;
import org.example.boottickets.app.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface TicketMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "created", ignore = true)
    TicketEntity fromTicketCreateRQ(
            final TicketCreateRQ req,
            final String name,
            final String content
    );

    @Mapping(target = "comments", source = "comments")
    TicketCreateRS toTicketCreateRS(final TicketEntity ticket);

    @Mapping(target = "comment", ignore = true)
    List<TicketGetAllRS> toListOfTicketGetByLogin(final List<TicketEntity> tickets);
}
