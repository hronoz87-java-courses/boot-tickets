package org.example.boottickets.app.mapper;

import org.example.boottickets.app.dto.UserRS;
import org.example.boottickets.app.entity.UserEntity;
import org.example.boottickets.app.security.ApplicationUserDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface UserEntityMapper {
  List<UserRS> toRS(final List<UserEntity> entities);

  @Mapping(target = "username", source = "login")
  @Mapping(target = "authorities", ignore = true)
  @Mapping(target = "accountNonLocked", ignore = true)
  @Mapping(target = "accountNonExpired", ignore = true)
  @Mapping(target = "credentialsNonExpired", ignore = true)
  @Mapping(target = "enabled", ignore = true)
  ApplicationUserDetails toUserDetails(final UserEntity saved);
}
