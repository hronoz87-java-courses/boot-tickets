package org.example.boottickets.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.boottickets.app.dto.TicketCreateRQ;
import org.example.boottickets.app.dto.TicketCreateRS;
import org.example.boottickets.app.dto.TicketGetAllRS;
import org.example.boottickets.app.security.ApplicationUserDetails;
import org.example.boottickets.app.service.TicketService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/ticket")
@RequiredArgsConstructor
public class TicketController {
    private final TicketService service;

    @GetMapping("getAll")
    public List<TicketGetAllRS> getAll(@AuthenticationPrincipal final ApplicationUserDetails principal,
                                       @RequestParam(defaultValue = "0") final int pageNo,
                                       @RequestParam(defaultValue = "50") final int limit) {
        return service.getAllTickets(principal, pageNo, limit);
    }

    @PostMapping("/create")
    public TicketCreateRS create(@AuthenticationPrincipal final ApplicationUserDetails principal,
                                @Valid @RequestBody final TicketCreateRQ req) {
      return  service.create(principal, req);
    }

    @GetMapping("getById")
    public TicketCreateRS getTicketById(@AuthenticationPrincipal final ApplicationUserDetails principal,
                                        @RequestParam final Long id) {
        return service.getTicketById(principal, id);
    }

    @GetMapping("closeTicket")
    public TicketCreateRS closeTicketById(@AuthenticationPrincipal final ApplicationUserDetails principal,
                                          @RequestParam final Long id) {
        return service.closeById(principal, id);
    }
}
