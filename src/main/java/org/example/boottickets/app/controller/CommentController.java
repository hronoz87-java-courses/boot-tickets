package org.example.boottickets.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.boottickets.app.dto.CommentCreateRQ;
import org.example.boottickets.app.dto.CommentCreateRS;
import org.example.boottickets.app.dto.TicketCreateRS;
import org.example.boottickets.app.dto.TicketGetAllRS;
import org.example.boottickets.app.security.ApplicationUserDetails;
import org.example.boottickets.app.service.CommentService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/comment")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService service;

    @GetMapping("/getAll")
    public List<TicketGetAllRS> getAll(@AuthenticationPrincipal final ApplicationUserDetails principal,
                                       @RequestParam(defaultValue = "0") final int pageNo,
                                       @RequestParam(defaultValue = "50") final int limit) {
        return service.getAllTickets(principal, pageNo, limit);
    }

    @PostMapping("/create")
    public CommentCreateRS create(@AuthenticationPrincipal final ApplicationUserDetails principal,
                                  @Valid @RequestBody  final CommentCreateRQ req, @RequestParam final Long id) {
        return service.createComment(principal, req, id);
    }

    @GetMapping("/getById")
    public TicketCreateRS getById(@AuthenticationPrincipal final ApplicationUserDetails principal, @RequestParam final Long id) {
        return service.getTicketById(principal, id);
    }
}
