package org.example.boottickets.app.controller;


import lombok.RequiredArgsConstructor;
import org.example.boottickets.app.security.ApplicationUserDetails;
import org.example.boottickets.app.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService service;

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ApplicationUserDetails getUserByLogin(@AuthenticationPrincipal ApplicationUserDetails principal) {
        return service.loadUserByUsername(principal.getUsername());
    }
}
