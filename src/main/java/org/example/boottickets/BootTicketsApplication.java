package org.example.boottickets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootTicketsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootTicketsApplication.class, args);
    }

}
