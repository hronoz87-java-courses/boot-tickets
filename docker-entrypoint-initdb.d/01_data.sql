INSERT INTO users(id, login, password, role, removed, created)
VALUES (0, 'sasha', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$VD2gbE9s9SvxSU3QnmeO9w$hosiwDgCWLdyZ6xrysnDg9fDE38frM65jxOj58ZkCXs','SUPPORT', false, CURRENT_TIMESTAMP),
       (1, 'petya', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$VD2gbE9s9SvxSU3QnmeO9w$hosiwDgCWLdyZ6xrysnDg9fDE38frM65jxOj58ZkCXs', 'USER', false, CURRENT_TIMESTAMP),
       (2, 'vasya', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$VD2gbE9s9SvxSU3QnmeO9w$hosiwDgCWLdyZ6xrysnDg9fDE38frM65jxOj58ZkCXs', 'USER', false, CURRENT_TIMESTAMP);
