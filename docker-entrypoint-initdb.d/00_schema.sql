CREATE TABLE users
(
    id       BIGSERIAL PRIMARY KEY,
    login    TEXT        NOT NULL UNIQUE,
    password TEXT        NOT NULL,
    role     TEXT        NOT NULL DEFAULT 'USER',

    removed  BOOLEAN     NOT NULL DEFAULT false,
    created  timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE ticket
(
    id       BIGSERIAL PRIMARY KEY,
    name     TEXT        NOT NULL,
    content  TEXT        NOT NULL,
    status   TEXT        NOT NULL DEFAULT 'OPEN',
    user_id  BIGSERIAL   NOT NULL REFERENCES users,
    comments TEXT        NOT NULL DEFAULT '[]',
    created  timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE comments
(
    id        BIGSERIAL PRIMARY KEY,
    author    TEXT        NOT NULL,
    text      TEXT        NOT NULL,
    ticket_id BIGSERIAL REFERENCES ticket,
    created   timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
);
